package com.aaljukic.interfaces;

public interface ProgramModeInterface {
    void startRunningProgram(String programMode);
    void sendMessage();
    void receiveMessage();

    void setAddress(String address);
    String getAddress();

    void setTCPSocketPort(int TCPSocketPort);
    int getTCPSocketPort();

    void setRateOfSendingMessages(int rateOfSendingMessages);
    int getRateOfSendingMessages();

    void setSizeOfMessage(int sizeOfMessage);
    int getSizeOfMessage();

    void setHostname(String hostname);
    String getHostname();

    void setCounterOfMessage(int counterOfMessage);
    int getCounterOfMessage();


}
