package com.aaljukic.modes;

public class Catcher extends ProgramModeAdapter {

    private String address;
    private int TCPSocketPort;

    public Catcher(String address, int TCPSocketPort) {
        this.address = address;
        this.TCPSocketPort = TCPSocketPort;
    }

    @Override
    public void setTCPSocketPort(int TCPSocketPort) {
        this.TCPSocketPort = TCPSocketPort;
    }

    @Override
    public int getTCPSocketPort() {
        return TCPSocketPort;
    }

    @Override
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String getAddress() {
        return address;
    }
}
