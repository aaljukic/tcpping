package com.aaljukic.modes;

import com.aaljukic.interfaces.ProgramModeInterface;

import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class ProgramMode {

    private ProgramModeInterface programMode;

    public ProgramMode(ProgramModeInterface programModeInterface) {
        this.programMode = programModeInterface;
    }

    /**
     * Program starts here based on passed arguments from main class "-c" or "-p"
     * **/
    public void startProgram(String chosenMode) throws Exception {
        if ("-c".equals(chosenMode)) {
            String[] separatedPitcherData;
            long initialCatcherPongTime;
            String response;
            boolean isRequest = true;
            ServerSocket serverSocket;

            System.out.println();
            System.out.println("|-------------------------------------------------------------|");
            System.out.printf("%10s %25s %15s", "MODE", "IP ADDRESS", "PORT");
            System.out.println();
            System.out.println("|-------------------------------------------------------------|");
            System.out.format("%12s %23s %15s", "Catcher", programMode.getAddress(), programMode.getTCPSocketPort());
            System.out.println();
            System.out.println("|-------------------------------------------------------------|");

            while (isRequest) {
                try {
                    serverSocket = new ServerSocket(programMode.getTCPSocketPort());
                    System.out.println("\nCatcher is listening for Pitcher request...");
                    Socket socket = serverSocket.accept();
                    System.out.println("\nPitcher is connected!");
                    String pitcherData = fetchingData(socket);
                    initialCatcherPongTime = System.nanoTime();

                    System.out.println();
                    System.out.println("|-------------------------------------------------------------|");
                    System.out.println("\nData: " + pitcherData);
                    separatedPitcherData = pitcherData.split("\\+");

                    response = formattingResponse(separatedPitcherData[0], separatedPitcherData[1], initialCatcherPongTime, separatedPitcherData[3]);

                    System.out.println("\nResponse: " + response);
                    System.out.println();
                    System.out.println("|-------------------------------------------------------------|");
                    sendingData(socket, response);

                    if (("T").equals(separatedPitcherData[3])) {
                        isRequest = false;
                    }

                    serverSocket.close();

                } catch (BindException exception) {
                    terminateProgramWithErrorMessage("\nPort is already in use. Please choose another one!");
                    exception.printStackTrace();
                }
            }
        } else if ("-p".equals(chosenMode)) {
            int totalPingRepetition = programMode.getRateOfSendingMessages() * 3,
                    currentPitcherPing = 1,
                    timeToWaitBeforeNextPing = 1000;

            long initialPingTime,
                    pongResponseTime,
                    secondPitcherPingTime;
            long roundTripTime = 0,
                    pitcherToCatcherTime = 0,
                    catcherToPitcherTime = 0,
                    sumOfAllInitialPingTimes = 0,
                    sumOfAllRoundTripTimes = 0,
                    sumOfAllPitcherToCatcherTimes = 0,
                    sumOfAllCatcherToPitcherTimes = 0;

            int numberOfSeparatorsInMessage = 3,
                    currentNumberOfMessage,
                    terminatorCharacterLength = 1;

            String textLengthInitialPingTime,
                    textLengthOfCurrentNumberOfMessage,
                    numberOfPackages;

            int counterOfMissingPackages,
                    receivedNumberOfPackage,
                    counterOfSentMessagesInOneSecond = 0,
                    counterOfAllSentMessages = 0,
                    numberOfSentMessagesInLastSecond;

            String[] separatedResponseData;

            List<Integer> sentPackagesNumbers = new ArrayList<>();
            List<Integer> receivedPackagesNumbers = new ArrayList<>();
            List<Long> maximumRoundTripTimes = new ArrayList<>();

            String hostIPAddress = findIPAddressByHostname(programMode.getHostname());
            System.out.println();
            System.out.println("|-------------------------------------------------------------------------------------------------------------|");
            System.out.printf("%25s %25s %25s %25s", "MODE", "HOSTNAME", "IP ADDRESS", "PORT");
            System.out.println();
            System.out.println("|-------------------------------------------------------------------------------------------------------------|");
            System.out.format("%25s %25s %25s %25s",
                    "Pitcher",
                    programMode.getHostname(),
                    hostIPAddress,
                    programMode.getTCPSocketPort()
            );
            System.out.println();
            System.out.println("|-------------------------------------------------------------------------------------------------------------|");
            System.out.println();

            while (currentPitcherPing < totalPingRepetition) {
                synchronized (this){
                    do {
                        // Establishing socket connection with Catchers socket
                        try {
                            System.out.println("|-------------------------------------------------------------------------------------------------------------|");
                            Socket pitcherSocket = new Socket(hostIPAddress, programMode.getTCPSocketPort());
                            System.out.println("\nPitcher is sending data...");

                            currentNumberOfMessage = programMode.getCounterOfMessage();
                            textLengthOfCurrentNumberOfMessage = Integer.toString(currentNumberOfMessage);

                            initialPingTime = System.nanoTime();
                            //sumOfAllInitialPingTimes += initialPingTime;
                            textLengthInitialPingTime = Long.toString(initialPingTime);

                            String data = generatingRandomContentForMessageWithArguments(
                                    (programMode.getSizeOfMessage() -
                                            (
                                                    textLengthInitialPingTime.length() +
                                                            numberOfSeparatorsInMessage +
                                                            textLengthOfCurrentNumberOfMessage.length() +
                                                            terminatorCharacterLength
                                            )
                                    ),
                                    programMode.getCounterOfMessage(),
                                    currentPitcherPing,
                                    textLengthInitialPingTime,
                                    totalPingRepetition
                            );

                            System.out.println("\nRequest: " + data);
                            sendingData(pitcherSocket, data);

                            sentPackagesNumbers.add(programMode.getCounterOfMessage());

                            // Fetching & save second Pitcher Ping time
                            String responseDataFromCatcher = fetchingData(pitcherSocket);

                            // A -> B -> A Time - ping
                            secondPitcherPingTime = System.nanoTime();

                            separatedResponseData = responseDataFromCatcher.split("\\+");
                            pongResponseTime = Long.parseLong(separatedResponseData[2]);

                            if (programMode.getCounterOfMessage() > 9) {
                                numberOfPackages = responseDataFromCatcher.substring(0, 2);
                            } else {
                                numberOfPackages = responseDataFromCatcher.substring(0, 1);
                            }

                            receivedNumberOfPackage = Integer.parseInt(numberOfPackages);
                            receivedPackagesNumbers.add(receivedNumberOfPackage);

                            // Counters
                            currentPitcherPing++;
                            programMode.setCounterOfMessage(programMode.getCounterOfMessage() + 1);

                            System.out.println("\n\nReply from: " + hostIPAddress +
                                    "\nResponse: " + responseDataFromCatcher);
                            System.out.println();
                            System.out.println("|-------------------------------------------------------------------------------------------------------------|");
                            System.out.println();

                            // Statistics
                            roundTripTime = secondPitcherPingTime - initialPingTime;
                            maximumRoundTripTimes.add(roundTripTime);
                            sumOfAllRoundTripTimes += roundTripTime;

                            pitcherToCatcherTime = pongResponseTime - initialPingTime;
                            sumOfAllPitcherToCatcherTimes += pitcherToCatcherTime;

                            catcherToPitcherTime = secondPitcherPingTime - pongResponseTime;
                            sumOfAllCatcherToPitcherTimes += catcherToPitcherTime;
                        } catch (ConnectException exception) {
                            System.out.println("Exception ==> " + exception.getMessage());

                            // Store number of sent messages
                            sentPackagesNumbers.add(programMode.getCounterOfMessage());

                            // Counters
                            System.out.println("\nPing: " + currentPitcherPing + ". Request timed out.");
                            currentPitcherPing++;
                            programMode.setCounterOfMessage(programMode.getCounterOfMessage() + 1);
                        } catch (SocketException exception) {
                            System.out.println("\n" + exception.getMessage());
                            terminateProgramWithErrorMessage("Catcher Terminated Your Requests");
                        }
                        counterOfSentMessagesInOneSecond++;

                    } while (counterOfSentMessagesInOneSecond < programMode.getRateOfSendingMessages());

                    numberOfSentMessagesInLastSecond = counterOfSentMessagesInOneSecond;
                    counterOfSentMessagesInOneSecond = 0;
                    //+=
                    counterOfAllSentMessages += numberOfSentMessagesInLastSecond;

                    // Console output after each round of pings ping
                    System.out.println();
                    System.out.println("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|");
                    System.out.printf("%10s %25s %35s %30s %30s %30s %30s", "|TIME|", "|NUMBER OF ALL SENT MESSAGES|",
                            "|NUMBER OF SENT MESSAGES IN PREVIOUS SECOND|", "|AVERAGE TIME FOR A -> B -> A|",
                            "|MAX TIME FOR A -> B -> A|", "|AVERAGE TIME FOR A -> B|", "|AVERAGE TIME FOR B -> A|");
                    System.out.println();
                    System.out.println("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|");
                    System.out.format("%11s %15s %40s %32s %30s %35s %30s",
                            getCurrentTime(),
                            counterOfAllSentMessages,
                            numberOfSentMessagesInLastSecond,
                            sumOfAllInitialPingTimes / numberOfSentMessagesInLastSecond,
                            Collections.max(maximumRoundTripTimes),
                            sumOfAllPitcherToCatcherTimes / numberOfSentMessagesInLastSecond,
                            sumOfAllCatcherToPitcherTimes / numberOfSentMessagesInLastSecond
                    );
                    System.out.println();
                    System.out.println("|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|");
                    System.out.println();

                    this.wait(timeToWaitBeforeNextPing);
                }
            }

            /*
                Compare lists after Pitcher is done &
                output missing packages if there is any
            */
            sentPackagesNumbers.removeAll(receivedPackagesNumbers);

            if (receivedPackagesNumbers.size() != totalPingRepetition) {
                counterOfMissingPackages = sentPackagesNumbers.size();
                System.out.println();
                System.out.println("|-------------------------------------------------------------------------------------------------------------|");
                System.out.format(      "%15s","All missing packets: { " + sentPackagesNumbers + " }");
                System.out.println();
                System.out.println("|-------------------------------------------------------------------------------------------------------------|");
                System.out.println();
                System.out.format(      "%15s","There is " + counterOfMissingPackages + " missing packets!");
                System.out.println();
                System.out.println("|-------------------------------------------------------------------------------------------------------------|");
                System.out.println();
            } else {
                System.out.println();
                System.out.println("|-------------------------------------------------------------------------------------------------------------|");
                System.out.println();
                System.out.format(      "%15s","There is no missing packets!");
                System.out.println();
                System.out.println("|-------------------------------------------------------------------------------------------------------------|");
                System.out.println();
            }
        }
    }

    private String generatingRandomContentForMessageWithArguments(int lengthOfMessage, int counterOfMessage, int pitcherPing, String textPingMilliseconds, int ping) {
            String terminator = "N";
            int leftLimit = 97;
            int rightLimit = 122;

            Random random = new Random();

            StringBuilder buffer = new StringBuilder(lengthOfMessage);
            for (int i = 0; i < lengthOfMessage; i++) {
                int randomLimitedInt = leftLimit + (int)
                        (random.nextFloat() * (rightLimit - leftLimit + 1));
                buffer.append((char) randomLimitedInt);
            }
            String generatedString = buffer.toString();

            if (pitcherPing == ping) {
                terminator = "T";
            }
            return counterOfMessage + "+" + generatedString + "+" + textPingMilliseconds + "+" + terminator;
    }

    private void sendingData(Socket socket, String data) throws IOException {
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(socket.getOutputStream());
        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
        printWriter.println(data);
        outputStreamWriter.flush();
    }
    private String fetchingData(Socket socket) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        return bufferedReader.readLine();
    }
    private String formattingResponse(String number, String content, long pongFromCatcher, String terminator) {
        return number + "+" + content + "+" + pongFromCatcher + "+" + terminator;
    }

    private String findIPAddressByHostname(String hostname) {
        try {
            System.out.println("\nHostname: " + hostname);
            InetAddress host = InetAddress.getByName(hostname);
            System.out.println("\nHost IP Address: " + host.getHostAddress());
            return host.getHostAddress();
        }catch (UnknownHostException exception) {
            terminateProgramWithErrorMessage("\nHost by name: " + hostname + " is not found.");
            exception.printStackTrace();
        }
        return null;
    }

    private static void terminateProgramWithErrorMessage (String message) {
        System.out.println(message);
        System.exit(-1);
    }

    private String getCurrentTime(){
        SimpleDateFormat format=new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        return format.format(new Date().getTime());
    }

}
