package com.aaljukic.modes;

import com.aaljukic.interfaces.ProgramModeInterface;

public class ProgramModeAdapter implements ProgramModeInterface {
    @Override
    public void startRunningProgram(String programMode) {
    }

    @Override
    public void sendMessage() {
    }

    @Override
    public void receiveMessage() {
    }

    @Override
    public int getTCPSocketPort() {
        return 0;
    }

    @Override
    public void setAddress(String address) {
    }

    @Override
    public String getAddress() {
        return null;
    }

    @Override
    public void setTCPSocketPort(int TCPSocketPort) {
    }

    @Override
    public void setRateOfSendingMessages(int rateOfSendingMessages) {
    }

    @Override
    public int getRateOfSendingMessages() {
        return 0;
    }

    @Override
    public void setSizeOfMessage(int sizeOfMessage) {
    }

    @Override
    public int getSizeOfMessage() {
        return 0;
    }

    @Override
    public void setHostname(String hostname) {
    }

    @Override
    public String getHostname() {
        return null;
    }

    @Override
    public void setCounterOfMessage(int counterOfMessage) {
    }

    @Override
    public int getCounterOfMessage() {
        return 0;
    }
}
