package com.aaljukic.modes;

public class Pitcher extends ProgramModeAdapter {

    private int TCPSocketPort, rateOfSendingMessages, sizeOfMessage, counterOfMessages;
    private String hostname;

    public Pitcher(int TCPSocketPort, int rateOfSendingMessages, int sizeOfMessage, int counterOfMessages, String hostname) {
        this.TCPSocketPort = TCPSocketPort;
        this.rateOfSendingMessages = rateOfSendingMessages;
        this.sizeOfMessage = sizeOfMessage;
        this.counterOfMessages = counterOfMessages;
        this.hostname = hostname;
    }

    @Override
    public void setTCPSocketPort(int TCPSocketPort) {
        this.TCPSocketPort = TCPSocketPort;
    }

    @Override
    public int getTCPSocketPort() {
        return TCPSocketPort;
    }

    @Override
    public void setRateOfSendingMessages(int rateOfSendingMessages) {
        this.rateOfSendingMessages = rateOfSendingMessages;
    }

    @Override
    public int getRateOfSendingMessages() {
        return rateOfSendingMessages;
    }

    @Override
    public void setSizeOfMessage(int sizeOfMessage) {
        this.sizeOfMessage = sizeOfMessage;
    }

    @Override
    public int getSizeOfMessage() {
        return sizeOfMessage;
    }

    @Override
    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    @Override
    public String getHostname() {
        return hostname;
    }

    @Override
    public void setCounterOfMessage(int counterOfMessage) {
        this.counterOfMessages = counterOfMessage;
    }

    @Override
    public int getCounterOfMessage() {
        return counterOfMessages;
    }
}
