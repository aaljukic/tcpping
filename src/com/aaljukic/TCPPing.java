package com.aaljukic;

import com.aaljukic.modes.Catcher;
import com.aaljukic.modes.Pitcher;
import com.aaljukic.modes.ProgramMode;

import java.util.HashMap;

public class TCPPing {

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            terminateProgramWithErrorMessage("\nThere were no commandline arguments passed!");
        }
        if (("-help").equals(args[0])) {
            hintMessage();
            terminateProgramWithErrorMessage("");
        }
        if (args.length != 5 && args.length != 8) {
            terminateProgramWithErrorMessage("\nWrong commandline arguments passed!");
        } else {
            for (int i = 0; i < args.length; i++) {
                args[i] = args[i].toLowerCase();
            }

            if (!"-c".equals(args[0]) && !"-p".equals(args[0])) {
                terminateProgramWithErrorMessage("\nWrong commandline arguments passed!");
            } else {
                validateCommandlineArgumentsAndSelectProgramMode(args);
            }
        }
    }

    /** Choose Program Mode **/
    private static void validateCommandlineArgumentsAndSelectProgramMode(String[] arguments) throws Exception {
        switch (arguments[0]) {
            case "-c":
                if (!"-bind".equals(arguments[1]) || !"-port".equals(arguments[3])) {
                    descriptiveMessageToNotifyUserAboutNextStep("Use \"TCPPing -help\" to see all possibilities for catcher mode.");
                    terminateProgramWithErrorMessage("\nWrong commandline arguments for mode \"-c\" passed!");
                } else {
                    selectProgramModeBasedOnPassedArguments(arguments[0], arguments[2], arguments[4]);
                }
                break;
            case "-p":
                if (!"-port".equals(arguments[1]) || !"-mps".equals(arguments[3]) || !"-size".equals(arguments[5])){
                    descriptiveMessageToNotifyUserAboutNextStep("Use \"TCPPing -help\" to see all possibilities for pitcher mode.");
                    terminateProgramWithErrorMessage("\nWrong commandline arguments for mode \"-p\" passed!");
                } else {
                    selectProgramModeBasedOnPassedArguments(arguments[0], arguments[2], arguments[4], arguments[6], arguments[7]);
                }
                break;
            default:
                terminateProgramWithErrorMessage("\nWrong commandline arguments passed!");
                throw new IllegalStateException("Unexpected value: " + arguments[0]);
        }
    }
    private static void selectProgramModeBasedOnPassedArguments(String chosenModeArgument, String socketBindAddress, String TCPSocketPort) throws Exception {
        if (validateNumberArguments(TCPSocketPort)) {
            descriptiveMessageToNotifyUserAboutNextStep("\nTCP Socket Port Argument Is Not A Number");
            terminateProgramWithErrorMessage("\nWrong commandline arguments passed!");
        } else {
            int port = Integer.parseInt(TCPSocketPort);

            ProgramMode catcher = new ProgramMode(new Catcher(socketBindAddress, port));

            catcher.startProgram(chosenModeArgument);
        }
    }
    private static void selectProgramModeBasedOnPassedArguments(String chosenModeArgument, String TCPSocketPort, String rateOfSendingMessages, String sizeOfMessage, String hostname) throws Exception {
        if (validateNumberArguments(TCPSocketPort) || validateNumberArguments(rateOfSendingMessages) || validateNumberArguments(sizeOfMessage)) {
            descriptiveMessageToNotifyUserAboutNextStep("\nTCP Socket Port, Rate Or Size Arguments Are Not A Numbers");
            terminateProgramWithErrorMessage("\nWrong commandline arguments passed!");
        } else {
            int rate = 1;
            rate = Integer.parseInt(rateOfSendingMessages);
            int port = Integer.parseInt(TCPSocketPort), size = Integer.parseInt(sizeOfMessage);

            if (!validateSizeOfMessage(size)) {
                descriptiveMessageToNotifyUserAboutNextStep("\nSize Of Message Is Less Then 50 Or Greater Then 3000 bytes!");
                size = 300;
            }

            ProgramMode pitcher = new ProgramMode(new Pitcher(port, rate, size,1, hostname));

            pitcher.startProgram(chosenModeArgument);
        }
    }

    private static void terminateProgramWithErrorMessage (String message) {
        System.out.println(message);
        System.exit(-1);
    }
    private static void descriptiveMessageToNotifyUserAboutNextStep (String message) {
        System.out.println(message);
        hintMessage();
    }
    private static void hintMessage() {
        String[] commands = {"-p", "-c", "-port <port>", "-bind <ip_address>", "-mps <rate>", "-size <size>", "<hostname>" };

        HashMap<String, String> listOfCommandsWithDescription = new HashMap<>();
        listOfCommandsWithDescription.put("-p", "\t\tNacin rada kao Pitcher" +
                "\n|-----------------------------------------------------------------------------------------------------------------------------------|");
        listOfCommandsWithDescription.put("-c", "\t\tNacin rada kao Catcher" +
                "\n|-----------------------------------------------------------------------------------------------------------------------------------|");
        listOfCommandsWithDescription.put("-port <port>", "\t[Pitcher] TCP socket port koji ce se koristiti za connect" +
                "\n\t\t\t\t\t\t[Catcher] TCP socket port koji e se koristiti za listen" +
                "\n|-----------------------------------------------------------------------------------------------------------------------------------|");
        listOfCommandsWithDescription.put("-bind <ip_address>", "[Catcher] TCP socket bind adresa na kojoj ce biti pokrenut listen" +
                "\n|-----------------------------------------------------------------------------------------------------------------------------------|");
        listOfCommandsWithDescription.put("-mps <rate>", "\t[Pitcher] Brzina slanja izrazena u \"messages per second\"" +
                "\n|-----------------------------------------------------------------------------------------------------------------------------------|");
        listOfCommandsWithDescription.put("-size <size>", "\t[Pitcher] duzina poruke: min = 50, max = 3000" +
                "\n|-----------------------------------------------------------------------------------------------------------------------------------|");
        listOfCommandsWithDescription.put("<hostname>", "\t[Pitcher] ime racunala na kojem je pokrenut Catcher");

        System.out.println();
        System.out.println("|-----------------------------------------------------------------------------------------------------------------------------------|");
        System.out.printf(                      "%20s %75s", "COMMAND", "DESCRIPTION");
        System.out.println();
        System.out.println("|-----------------------------------------------------------------------------------------------------------------------------------|");

        listOfCommandsWithDescription.forEach((k,v) ->
                System.out.println("|\t" + k + "\t\t\t" + v)
        );
        System.out.println();
        System.out.println("|-----------------------------------------------------------------------------------------------------------------------------------|");
    }

    private static boolean validateNumberArguments(String argument) {
        return !argument.matches("^\\d+$");
    }
    private static boolean validateSizeOfMessage(int size) {
        return size >= 50 && size <= 3000;
    }
}
